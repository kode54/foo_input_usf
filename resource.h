//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by psf.rc
//
#define IDD_PSF_CONFIG                  101
#define IDD_TIME                        102
#define IDR_BIOS                        103
#define IDC_INDEFINITE                  1000
#define IDC_DLENGTH                     1001
#define IDC_DFADE                       1002
#define IDC_SOS                         1003
#define IDC_SES                         1004
#define IDC_SILENCE                     1005
#define IDC_HLE_AUDIO                   1006
#define IDC_RESAMPLE_HZ                 1007
#define IDC_URL                         1012
#define IDC_K54                         1013
#define IDC_LENGTH                      1015
#define IDC_FADE                        1016
#define IDC_INFO                        1018
#define IDC_RESAMPLE                    1019

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        123
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1020
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
